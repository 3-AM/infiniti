$(document).ready(function(){


	if ($('#main-slider').length){
		var slider = $('#main-slider');
		var carMod = [];

		$.each(slider.find('.slides').children().not('.clone'), function() {
			carMod.push($(this).find('.slide-descr .title').text());
		});

		$.each(slider.find('.flex-control-nav').children(), function(i) {
			$(this).find('a').text(carMod[i]);
		})
	}

	if ($('header').find('.model-list').length){
		var link = $('header').find('.model-list').find('[data-model]');
		var models = $('header').find('.model-list').find('.model-descr');

		link.on('mouseenter', function(){
			var model = $(this).siblings('.model-descr');
			var offsLeft = null;

			models.hide();

			if (model){
				offsLeft = $(this).position().left;
				model.find('.bg-line').css({
					backgroundPosition: offsLeft - 934  + $(this).width()/2 
				});
				$('.overlay').fadeIn(200);
				model.show();
			};
		});

		$('.head-main').on('mouseleave', function(){
			$('.overlay').fadeOut(200);
			models.hide();
		});
	}
});