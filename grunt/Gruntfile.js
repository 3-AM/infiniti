module.exports = function(grunt) {

	grunt.initConfig({
		pkg: grunt.file.readJSON('package.json'),

		compass: {
			dist: {
				options: {
					noLineComments: true,
					sassDir: '../scss',
					cssDir: '../css',
					imagesDir: '../img/css/',
					httpGeneratedImagesPath: '../img/css/'
				}
			}
		},

		watch: {
			css: {
				files: '../scss/*.scss',
				tasks: ['compass']
			},

			options: {
				livereload: 'true'
			},
		},

		imagemin: {
			png: {
				options: {
					optimizationLevel: 7
				},
				files: [
				{
					expand: true,
					cwd: '../img/css/icons',
					src: '*.png',
					dest: '../img/css/icon-min',
					ext: '.png'
				}
				]
			}
		},

		uncss: {
			dist: {
				options: {
					media        : ['all and (transform-3d), (-webkit-transform-3d)'],
					csspath      : '../bootstrap1',
		      		timeout      : 1000,
		      		htmlroot     : 'bootstrap1',
		      		report       : 'min'
		      	},
		      	files: {
		      		'../css/stylesheets.css': ['../index.html']
		      	}
		      }
		  },

		  cmq: {
		  	options: {
		  		log: false
		  	},
		  	your_target: {
		  		files: {
		  			'../css': ['../css/stylesheets.css']
		  		}
		  	}
		  },

		});

	// 3. Тут мы указываем Grunt, что хотим использовать этот плагин
	grunt.loadNpmTasks('grunt-contrib-watch');
	grunt.loadNpmTasks('grunt-contrib-compass');
	grunt.loadNpmTasks('grunt-combine-media-queries');
	grunt.loadNpmTasks('grunt-contrib-imagemin');
	grunt.loadNpmTasks('grunt-uncss');

	// 4. Указываем, какие задачи выполняются, когда мы вводим «grunt» в терминале
	grunt.registerTask('default', ['uncss', 'cmq']);
	grunt.registerTask('imagepng', ['imagemin:png']);
	grunt.registerTask('watch', ['watch']);
};